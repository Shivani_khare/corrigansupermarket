package com.ait.jsf;

import static org.junit.Assert.*;
import org.junit.Ignore;
import org.junit.Test;
import com.paypal.api.payments.Payment;
import com.paypal.base.rest.PayPalRESTException;

public class PaymentServicesTest{
    private PaymentServices paymentServices = new PaymentServices();

    @SuppressWarnings("unused")
	@Test
    public void testWhenDetailsCorrect_AuthorizePaymentOk() throws PayPalRESTException{
        float subtotal = 5.0f, shipping = 3.0f, tax = 2.0f, total = subtotal + shipping + tax; // Given
        OrderDetail orderDetails = new OrderDetail("product-name", subtotal, shipping, tax); // When
        String authorizeResult = paymentServices.authorizePayment(orderDetails);
        assertNotNull(authorizeResult); // Then
    }

    @Ignore
    @Test
    public void testWhenDetailsIncorrect_AuthorizePaymentFails() throws PayPalRESTException{
       // To be considered - no immediate means to create an invalid test scenario
    }

    @Ignore
    @Test
    // Could not get it to work - error: Invalid Resource ID
    public void testExecutePaymentDetails() throws PayPalRESTException{
        String paymentId = "id-1", payerId = "user-id-bob"; // Given
        Payment payment = paymentServices.executePayment(paymentId, payerId); // When
        assertEquals("created", payment); // Then
    }
}