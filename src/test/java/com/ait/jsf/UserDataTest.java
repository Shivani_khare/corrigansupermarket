package com.ait.jsf;

import static org.junit.Assert.fail;
import static org.junit.jupiter.api.Assertions.*;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class UserDataTest{
	UserData a = new UserData();	
	public ArrayList<User> users = new ArrayList<>();

	public void init(){
		users = new ArrayList<User>();
		User firstUser = new User("William", "william40");
		firstUser.setStaff(true);
		users.add(firstUser);

		User secondUser = new User("Harry", "harry30");
		users.add(secondUser);

		User thirdUser = new User("Jon", "jon2021");
		users.add(thirdUser);
	}

	@Test
	public void initTest(){
		a.init();
	}

	@Test
	public void validateUserTest(){
		a.init();
		assertTrue(a.validateUser("Harry", "harry30"));
	}

	@Test
	public void addUserTest(){
		a.init();
		User b = new User("Damien", "Pass");
		assertTrue(a.addUser(b));
	}

	@Test
	public void setRedirectPageTest(){
		a.setRedirectPage("/");
		assertEquals("/", a.getRedirectPage());
	}

	@Test
	public void setUserTest(){
		User b = new User("Damien", "Pass");
		a.setCurrentUser(b);
		assertEquals(b, a.getUser());
	}

	@Test
	public void setCurrentUserTest(){
		User b = new User("Damien", "Pass");
		a.setCurrentUser(b);
		assertEquals(b, a.getCurrentUser());
	}
}