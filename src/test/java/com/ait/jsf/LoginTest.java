package com.ait.jsf;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;
import javax.faces.context.FacesContext;
import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class LoginTest{
	LoginBean a = new LoginBean();

	@Test
	public void setUserNameTest(){
		a.setUserName("user");
		assertEquals("user", a.getUserName());
	}

	@Test
	public void setPasswordTest(){
		a.setPassword("password");
		assertEquals("password", a.getPassword());
	}

	@Test
	public void setUserLoggedInTest(){
		a.setUserLoggedIn(true);
		assertTrue(a.isUserLoggedIn());
	}

	@Test
	public void setStaffLoggedInTest(){
		a.setStaffLogin(true);
		assertTrue(a.isStaffLogin());
	}
}