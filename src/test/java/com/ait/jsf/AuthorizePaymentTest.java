package com.ait.jsf;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class AuthorizePaymentTest{
    private AuthorizePayment actualAuthorizePayment;

    @Before
    public void setUp(){
        actualAuthorizePayment = new AuthorizePayment();
    }

    @Test
    public void testChangeProduct(){
        actualAuthorizePayment.setProduct("Milk");
        assertEquals("Milk", actualAuthorizePayment.getProduct());
    }

    @Test
    public void testGetSubtotal(){
        assertEquals(0, actualAuthorizePayment.getSubTotal(), 0.001);
    }

    @Test
    public void testChangeSubtotal(){
        actualAuthorizePayment.setSubTotal(5.0f);
        assertEquals(5.0f, actualAuthorizePayment.getSubTotal(), 0.001);
    }

    @Test
    public void testGetShipping(){
        assertEquals(0, actualAuthorizePayment.getShipping(), 0.001);
    }

    @Test
    public void testChangeShipping(){
        actualAuthorizePayment.setShipping(2.0f);
        assertEquals(2.0f, actualAuthorizePayment.getShipping(), 0.001);
    }

    @Test
    public void testGetTax(){
        assertEquals(0, actualAuthorizePayment.getTax(), 0.001);
    }

    @Test
    public void testChangeTax(){
        actualAuthorizePayment.setTax(1.0f);
        assertEquals(1.0f, actualAuthorizePayment.getTax(), 0.001);
    }

    @Test
    public void testGetTotal(){
        assertEquals(0, actualAuthorizePayment.getTotal(), 0.001);
    }

    @Test
    public void testChangeTotal(){
        actualAuthorizePayment.setTotal(8.0f);
        assertEquals(8.0f, actualAuthorizePayment.getTotal(), 0.001);
    }
}