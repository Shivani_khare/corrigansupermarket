package com.ait.jsf;

import static org.junit.Assert.*;
import org.junit.Test;

public class OrderDetailTest{
    @Test
    public void testConstructor(){
        String name = "Milk"; // Given
        float subtotal = 5.0f, shipping = 3.0f, tax = 2.00f;

        OrderDetail orderDetail = new OrderDetail(name, subtotal, shipping, tax); // When

        // Then
        float delta = 0.001f;
        assertEquals(name, orderDetail.getProductName());
        assertEquals(subtotal, Float.parseFloat(orderDetail.getSubtotal()), delta);
        assertEquals(shipping, Float.parseFloat(orderDetail.getShipping()), delta);
        assertEquals(tax, Float.parseFloat(orderDetail.getTax()), delta);

        float expectedTotal = subtotal + shipping + tax;
        assertEquals(expectedTotal, Float.parseFloat(orderDetail.getTotal()), delta);
    }
}