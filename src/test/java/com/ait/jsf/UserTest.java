package com.ait.jsf;

import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Test;

public class UserTest{
	public User a = new User("user", "pass");
	public Product product = new Product(12345, "Apple", 100, "Medium Red Apple", "Fruit", 0.50);
	public Product productA = new Product(12340, "Banana", 10, "Large Banana", "Fruit", 0.75);
	public List<Product> productList = new ArrayList<Product>();

	@Test
	public void getUserNameTest(){
		assertEquals("user", a.getUserName());
	}

	@Test
	public void setUserNameTest(){
		a.setUserName("newUser");
		assertEquals("newUser", a.getUserName());
	}

	@Test
	public void setSubTotalTest(){
		a.setSubTotal(1.0);
		assertEquals(1.0, a.getSubTotal(), 0.1);
	}

	@Test
	public void setGrandTotalTest(){
		productList.add(product);
		a.setProductList(productList);

		a.setGrandTotal(a.getGrandTotal());
		assertEquals(50.0, a.getGrandTotal(), 0.1);
	}

	@Test
	public void setPasswordTest(){
		a.setPassword("newPass");
		assertEquals("newPass", a.getPassword());
	}

	@Test
	public void setReenterPasswordTest(){
		a.setReenterPassword("newUserPass");
		assertEquals("newUserPass", a.getReenterPassword());
	}

	@Test
	public void setFirstNameTest(){
		a.setFirstName("Damien");
		assertEquals("Damien", a.getFirstName());
	}

	@Test
	public void setLastNameTest(){
		a.setLastName("Hunter");
		assertEquals("Hunter", a.getLastName());
	}

	@Test
	public void setEmailTest(){
		a.setEmail("damien.hunter9@gmail.com");
		assertEquals("damien.hunter9@gmail.com", a.getEmail());
	}

	@Test
	public void setAddressTest(){
		a.setAddress("Athlone");
		assertEquals("Athlone", a.getAddress());
	}

	@Test
	public void getProductListTest(){
		assertEquals(productList, a.getProductList());
	}

	@Test
	public void addProductTest(){
		product = new Product(12340, "Banana", 10, "Large Banana", "Fruit", 0.75);
		productList.add(product);
		a.addProduct(product);

		productList.add(productA);
		a.addProduct(productA);
		assertEquals(productList, a.getProductList());
	}

	@Test
	public void removeProductTest(){
		a.removeProduct(product);
		productList.remove(product);
		assertEquals(productList, a.getProductList());
	}
}