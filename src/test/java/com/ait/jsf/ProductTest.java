package com.ait.jsf;

import static org.junit.Assert.assertEquals;
import org.junit.jupiter.api.Test;

public class ProductTest{
	public Product a = new Product(12345, "Apple", 100, "Medium Red Apple", "Fruit", 0.50);

	public ProductTest(){}

	@Test
	public void getProdIdTest(){
		assertEquals(12345, a.getProdId());
	}

	@Test
	public void getProdNameTest(){
		assertEquals("Apple", a.getProdName());
	}

	@Test
	public void setProdNameTest(){
		a.setProdName("Red Apple");
		assertEquals("Red Apple", a.getProdName());
	}

	@Test
	public void getQuantityTest(){
		assertEquals(100, a.getQuantity());
	}

	@Test
	public void setQuantityTest(){
		a.setQuantity(150);
		assertEquals(150, a.getQuantity());
	}

	@Test
	public void getDescriptionTest(){
		assertEquals("Medium Red Apple", a.getDescription());
	}

	@Test
	public void setDescriptionTest(){
		a.setDescription("Large Red Apple");
		assertEquals("Large Red Apple", a.getDescription());
	}

	@Test
	public void getCategoryTest(){
		assertEquals("Fruit", a.getCategory());
	}

	@Test
	public void setCategoryTest(){
		a.setCategory("Veg");
		assertEquals("Veg", a.getCategory());
	}

	@Test
	public void getPriceTest(){
		assertEquals(0.5, a.getPrice(), 0.1);
	}

	@Test
	public void setPriceTest(){
		a.setPrice(1.00);
		assertEquals(1.00, a.getPrice(), 0.1);
	}
}