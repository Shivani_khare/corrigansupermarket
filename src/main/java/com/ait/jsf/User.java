package com.ait.jsf;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@RequestScoped
public class User{
	private String userName, password, reenterPassword, firstName, lastName, email, address;
	List<Product> productList = new ArrayList<Product>();
	private double subTotal, grandTotal;
	private boolean staff;

	public User(){
		super();
	}

	public User(String userName, String password){
		this.userName = userName;
		this.password = password;
	}

	public String getUserName(){
		return userName;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public double getSubTotal(){
		return subTotal;
	}

	public void setSubTotal(double subTotal){
		this.subTotal = subTotal;
	}

	public boolean isStaff(){
		return staff;
	}

	public void setStaff(boolean staff){
		this.staff = staff;
	}

	public double getGrandTotal(){
		double temp = 0;

		for(Product product:productList){
			temp += product.getSubTotal();
		}
		grandTotal = temp;
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal){
		this.grandTotal = grandTotal;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public String getReenterPassword(){
		return reenterPassword;
	}

	public void setReenterPassword(String reenterPassword){
		this.reenterPassword = reenterPassword;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getEmail(){
		return email;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getAddress(){
		return address;
	}

	public void setAddress(String address){
		this.address = address;
	}

	public List<Product> getProductList(){
		return productList;
	}

	public void setProductList(List<Product> productList){
		this.productList = productList;
	}

	public String registerUser(){
		FacesContext context = FacesContext.getCurrentInstance();
		UserData userData = Helper.getBean("userData", UserData.class);
		boolean formInvalid = false;

		if(!password.contentEquals(reenterPassword)){			
			FacesMessage errorMessage = new FacesMessage ("Passwords do not match");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("registrationForm:reenterPasswordForm", errorMessage);
			formInvalid = true;
		}

		if(userName.length() < 6){			
			FacesMessage errorMessage = new FacesMessage ("UserName should be a minimum of 6 characters");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR); 
			context.addMessage("registrationForm:userNameForm", errorMessage);
			formInvalid = true;
		}

		if(userData.getUsers().size() > 0){
			for(User user : userData.getUsers()){
				if(user.getUserName().equalsIgnoreCase(userName)){	
					FacesMessage errorMessage = new FacesMessage("UserName already exists. please choose a different UserName");
					errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("registrationForm:userNameForm", errorMessage);
					formInvalid = true;
				}
			}
		}

		if(formInvalid){
			return (null);
		}
		else{
			userData.addUser(this);
			return "registered.xhtml";
		}
	}

	public String validatePassword(){
		FacesContext context=FacesContext.getCurrentInstance();

		if(!(password.length() > 10)){
			FacesMessage errorMessage = new FacesMessage ("Not a Strong password");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("registrationForm:passwordForm", errorMessage);
			return (null);
		}
		else{
			return "";
		}
	}

	public String checkSamePassword(){
		if(password.contentEquals(reenterPassword)){
			FacesContext context = FacesContext.getCurrentInstance();
			FacesMessage errorMessage = new FacesMessage("Passwords do not match");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("loginForm", errorMessage);
			return (null);
		}
		return "";
	}

	public void removeProduct(Product item){
		for(Product product : productList){
			 if(product.getProdId() == item.getProdId()){
				 System.out.println("Deleting item " + product.getProdId());
				 productList.remove(item);
				 FacesContext.getCurrentInstance().addMessage("productForm:outputMsg", new FacesMessage("Product " + item.getProdId() + " Removed from Cart"));
				 break;
			 }
		 }
	 }

	public void addProduct(Product item){
		this.productList.add(item);
	}
}