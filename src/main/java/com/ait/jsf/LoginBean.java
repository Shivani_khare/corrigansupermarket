package com.ait.jsf;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class LoginBean{
	String userName, password;
	boolean userLoggedIn = false, staffLogin = false;
	User user;

	public String getUserName(){
		return userName;
	}

	public boolean isUserLoggedIn(){
		return userLoggedIn;
	}

	public void setUserLoggedIn(boolean userLoggedIn){
		this.userLoggedIn = userLoggedIn;
	}

	public void setUserName(String userName){
		this.userName = userName;
	}

	public String getPassword(){
		return password;
	}

	public void setPassword(String password){
		this.password = password;
	}

	public boolean isStaffLogin(){
		return staffLogin;
	}

	public void setStaffLogin(boolean staffLogin){
		this.staffLogin = staffLogin;
	}

	public String validateUserLogin(){
		UserData userData = Helper.getBean("userData", UserData.class);
		FacesContext context = FacesContext.getCurrentInstance();

		if(userData.validateUser(userName, password)){				
			if(this.staffLogin){
				if(userData.getCurrentUser().isStaff()){
					this.setUserLoggedIn(true);	
					return "staff.xhtml?faces-redirect=true";
				}
				else{
					FacesMessage errorMessage = new FacesMessage("User is not registered as a staff");
					errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
					context.addMessage("loginForm:panelGrid", errorMessage);
					return (null);
				}
			}
			this.setUserLoggedIn(true);	

			if(userData.getRedirectPage() != null)
				return userData.getRedirectPage();
			return "index.xhtml?faces-redirect=true";
		}
		else{
			FacesMessage errorMessage = new FacesMessage ("Invalid Username/Password Combination");
			errorMessage.setSeverity(FacesMessage.SEVERITY_ERROR);
			context.addMessage("loginForm:panelGrid", errorMessage);
			return (null);
	    }
	}

	public void showLoginMessage(){
		if(userLoggedIn){
			String msg = staffLogin ? "Logged in as Staff" : "Logged in as Customer";
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(msg));
		}
	}

	public String logout(){		
		this.userLoggedIn = false;
		this.staffLogin = false;
		UserData userData = Helper.getBean("userData", UserData.class);

		if(userData.getCurrentUser() != null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("User : " + userData.getCurrentUser().getUserName() + " Logged out"));
			userData.setCurrentUser(null);	
		}
		return "index.xhtml?faces-redirect=true";		
	}
}