package com.ait.jsf;

import java.util.ArrayList;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.RowEditEvent;

@ManagedBean
@RequestScoped
public class Product{
	private int quantity, prodId;
	private String description, category, prodName;
	private double price, subTotal, grandTotal;
	private static List<Product> productList = new ArrayList<>();;

	public Product(){
		super();
	}

	public Product(int prodId, String prodName, int quantity, String description, String category, double price){
		super();
		this.prodId = prodId;
		this.prodName = prodName;
		this.quantity = quantity;
		this.description = description;
		this.category = category;
		this.price = price;
	}

	public int getProdId(){
		return prodId;
	}

	public String getProdName(){
		return prodName;
	}

	public void setProdName(String prodName){
		this.prodName = prodName;
	}

	public int getQuantity(){
		return quantity;
	}

	public void setQuantity(int quantity){
		this.quantity = quantity;
	}

	public String getDescription(){
		return description;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getCategory(){
		return category;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public double getPrice(){
		return price;
	}

	public void setPrice(double price){
		this.price = price;
	}

	public List<Product> getProductList(){
		return productList;
	}

	public void setSubTotal(){
		subTotal = price * quantity;
	}

	public double getSubTotal(){
		subTotal = price * quantity;
		return subTotal;
	}

	public double getGrandTotal(){
		for(Product product : productList){
			grandTotal += product.getSubTotal();
		}
		return grandTotal;
	}

	public void setGrandTotal(double grandTotal){
		this.grandTotal = grandTotal;
	}

	public String addProduct(){	
		int lastProdId = 0;	

		lastProdId = productList.size() == 0 ? 999 : productList.get(productList.size() - 1).prodId;		
		productList.add(new Product(++lastProdId, prodName, quantity, description, category, price));
		FacesContext.getCurrentInstance().addMessage("productForm:outputMsg", new FacesMessage("Product Added"));
		return (null);
	}

	public void onRowEdit(RowEditEvent<Product> event){
		FacesMessage msg = new FacesMessage("Product Edited", String.valueOf(event.getObject().getProdId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onRowCancel(RowEditEvent<Product> event){
		FacesMessage msg = new FacesMessage("Edit Cancelled", String.valueOf(event.getObject().getProdId()));
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void onQuantityEdit(){
		FacesMessage msg = new FacesMessage("Product Edited");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

	public void deleteRow(Product item){
		for(Product product : productList){
			if(product.getProdId() == item.getProdId()){
				System.out.println("Deleting item " + product.getProdId());
				productList.remove(item);
				FacesContext.getCurrentInstance().addMessage("productForm:outputMsg", new FacesMessage("Product " + item.prodId + " Deleted"));
				break;
			}
		}
	}

	public void editRow(){
		System.out.println("Reached Edit");
	}
}