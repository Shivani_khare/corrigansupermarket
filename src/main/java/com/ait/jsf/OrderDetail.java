package com.ait.jsf;

import javax.faces.bean.ManagedBean;

@ManagedBean
public class OrderDetail{
    private String productName;
    private double subtotal;
    private float shipping, tax, total;

    public OrderDetail(String productName, float subtotal, float shipping, float tax){
        this.productName = productName;
        this.subtotal = subtotal;
        this.shipping = shipping;
        this.tax = tax;
        this.total = subtotal + shipping + tax;
    }

    public String getProductName(){
        return productName;
    }

    public String getSubtotal(){
        return String.format("%.2f", subtotal);
    }

    public String getShipping(){
        return String.format("%.2f", shipping);
    }

    public String getTax(){
        return String.format("%.2f", tax);
    }

    public String getTotal(){
        return String.format("%.2f", total);
    }
}