package com.ait.jsf;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

public class Helper{
    public static <T> T getBean(String beanName, Class<T> type){
        FacesContext context = FacesContext.getCurrentInstance(); // Get the FacesContext object.
        Application application = context.getApplication(); // Get the Application object for the current context.

        // Evaluate an EL expression, to get the bean with the specified name.
        return application.evaluateExpressionGet(context, "#{" + beanName + "}", type);
    }

	public static void expungeSession(){
        FacesContext context = FacesContext.getCurrentInstance(); // Get the FacesContext object.
        HttpSession session = (HttpSession)context.getExternalContext().getSession(false); // Get the HttpSession object for the current context.

        if(session != null){ // Invalidate (i.e. expunge) session state.
            session.invalidate();
        }
    }
}