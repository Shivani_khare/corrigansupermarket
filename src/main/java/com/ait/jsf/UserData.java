package com.ait.jsf;

import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@SessionScoped
public class UserData{
	public ArrayList<User> users;
	private User currentUser;
	private String redirectPage;

	@PostConstruct
	public void init(){
		users = new ArrayList<User>();
		User firstUser = new User("William", "william40");
		firstUser.setStaff(true);
		users.add(firstUser);

		User secondUser = new User("Harry", "harry30");
		users.add(secondUser);

		User thirdUser = new User("Jon", "jon2021");
		users.add(thirdUser);
	}

	public boolean validateUser(String userName, String password){
		boolean validUser = false;

		for(User user:users){
			if((user.getUserName().equals(userName)) && (user.getPassword().equals(password))){
				validUser = true;
				this.currentUser = user;
				break;
			}
		}
		return validUser;
	}

	public boolean addUser(User user){
		this.users.add(user);
		return true;
	}

	public ArrayList<User> getUsers(){
		return users;
	}

	public User getUser(){
		return currentUser;
	}

	public User getCurrentUser(){
		return currentUser;
	}

	public void setCurrentUser(User currentUser){
		this.currentUser = currentUser;
	}

	public User getUser(String userName){
		for(User user : users){
			if(userName.equals(user.getUserName())){
				System.out.println("User Found");
				return user;
			}
		}
		return null;
	}

	public String getRedirectPage(){
		return redirectPage;
	}

	public void setRedirectPage(String redirectPage){
		this.redirectPage = redirectPage;
	}

	public String addProduct(Product product){
		if(this.currentUser == null){
			System.out.print("usert not logged");
			redirectPage = "addToCart.xhtml";
			FacesContext.getCurrentInstance().addMessage("loginForm:outputMsg", new FacesMessage("Please Login First"));
			return "loginPage.xhtml";
		}

		if(product.getQuantity() < 1){
			System.out.print("Quantity less than 1");
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Oops!!!, Quantity cannot be less than 1"));
			return (null);
		}

		System.out.println(product.getProdId());

		for(Product prod: this.currentUser.productList){
			if(prod.getProdId() == product.getProdId()){
				System.out.println("Product Found. Changing quantity");
				prod.setQuantity(product.getQuantity());
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product Already Available. Changing Quantity"));
				return (null);
			}
		}
		this.currentUser.addProduct(product);

		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Product " + product.getProdName() + " Added"));
		return (null);
	}
}